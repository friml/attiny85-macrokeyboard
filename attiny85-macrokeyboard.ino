#define DEBUG_MSG_FUNCTIONS 0 //1 for True, 0 for False
#define DEBUG_MSG_VALUES    0 //1 for True, 0 for False
#define ANALOG_PIN 1          //1 for attiny85, A0 for arduino UNO
#define LED_PIN    1          //1 for attiny85, 13 for arduino UNO
#define TRIGGER_ON_RELEASE  0 //1 for trigger on release, 0 for trigger on press

#include "DigiKeyboard.h"

typedef enum Buttons {noButton, button1, button2, button3, button4, button5, button6, button7, button8, button9, error} Button;
Buttons buttonPressed = noButton;
Buttons buffer[8] = {noButton, button1, button2, button3, button4, button5, button6, button7};
byte bufferIndex = 0;

inline void button1function(void) {
    DigiKeyboard.sendKeyStroke(KEY_D, MOD_GUI_LEFT); //minimize all windows
}
inline void button2function(void) {
    DigiKeyboard.println("?LINE\n");
}
inline void button3function(void) {
    DigiKeyboard.println("?CIRCLE\n");
}
inline void button4function(void) {
    DigiKeyboard.println("button $ pressed\n");
}
inline void button5function(void) {
    DigiKeyboard.println("button % pressed\n");
}
inline void button6function(void) {
    DigiKeyboard.println("button ^ pressed\n");
}
inline void button7function(void) {
    DigiKeyboard.println("button & pressed\n");
}
inline void button8function(void) {
    DigiKeyboard.println("button * pressed\n");
}
inline void button9function(void) {
    DigiKeyboard.println("button ( pressed\n");
}

inline void buttonAction(Buttons but){
#if DEBUG_MSG_FUNCTIONS == 1
    DigiKeyboard.println("function buttonAction()");
#endif //DEBUG_MSG_FUNCTION
#if DEBUG_MSG_VALUES == 1
    DigiKeyboard.print("but: ");
    DigiKeyboard.println(String(but));
#endif
    DigiKeyboard.update();
    DigiKeyboard.sendKeyStroke(0);
    switch(but){
        case button1:
            button1function();
            break;
        case button2:
            button2function();
            break;
        case button3:
            button3function();
            break;
        case button4:
            button4function();
            break;
        case button5:
            button5function();
            break;
        case button6:
            button6function();
            break;
        case button7:
            button7function();
            break;
        case button8:
            button8function();
            break;
        case button9:
            button9function();
            break;
        default:
            return;
    DigiKeyboard.delay(100);
    }
}

void writeBuffer(Buttons but){
#if DEBUG_MSG_FUNCTIONS == 1
    DigiKeyboard.println("function writeBuffer()");
#endif
    buffer[bufferIndex++] = but;
    if(bufferIndex >= 8)
        bufferIndex = 0;
#if DEBUG_MSG_VALUES == 1
    DigiKeyboard.print("buffer: {");
    DigiKeyboard.print(String(buffer[0]));
    DigiKeyboard.print(", ");
    DigiKeyboard.print(String(buffer[1]));
    DigiKeyboard.print(", ");
    DigiKeyboard.print(String(buffer[2]));
    DigiKeyboard.print(", ");
    DigiKeyboard.print(String(buffer[3]));
    DigiKeyboard.print(", ");
    DigiKeyboard.print(String(buffer[4]));
    DigiKeyboard.print(", ");
    DigiKeyboard.print(String(buffer[5]));
    DigiKeyboard.print(", ");
    DigiKeyboard.print(String(buffer[6]));
    DigiKeyboard.print(", ");
    DigiKeyboard.print(String(buffer[7]));
    DigiKeyboard.println("}");
    DigiKeyboard.print("bufferIndex: ");
    DigiKeyboard.println(String(bufferIndex));
#endif
}

inline void bufferAppendNew(void){
#if DEBUG_MSG_FUNCTIONS == 1
    DigiKeyboard.println("function bufferAppend()");
#endif
  int analogValue = analogRead(ANALOG_PIN);
#if DEBUG_MSG_VALUES == 1
    DigiKeyboard.print("analogValue: ");
    DigiKeyboard.println(String(analogValue));
#endif
  if(analogValue>=870){
    writeBuffer(button9);
  } else if(analogValue>=768){
    writeBuffer(button8);
  } else if(analogValue>=665){
    writeBuffer(button7);
  } else if(analogValue>=563){
    writeBuffer(button6);
  } else if(analogValue>=460){
    writeBuffer(button5);
  } else if(analogValue>=359){
    writeBuffer(button4);
  } else if(analogValue>=256){
    writeBuffer(button3);
  } else if(analogValue>=153){
    writeBuffer(button2);
  } else if(analogValue>=51){
    writeBuffer(button1);
  } else {
    writeBuffer(noButton);
  }
}

inline Buttons readBuffer(void){
#if DEBUG_MSG_FUNCTIONS == 1
    DigiKeyboard.println("function readBuffer()");
#endif
    if(buffer[0] == buffer[1] &&
       buffer[1] == buffer[2] &&
       buffer[2] == buffer[3] &&
       buffer[3] == buffer[4] &&
       buffer[4] == buffer[5] &&
       buffer[5] == buffer[6] &&
       buffer[6] == buffer[7])
        return buffer[0];
    return error;
}

void setup() {
    // Initialize the LED pin as output
    pinMode(LED_PIN, OUTPUT);
    // Initialize the input pin as input
    pinMode(ANALOG_PIN+1, INPUT);
    digitalWrite(LED_PIN, LOW);
    digitalWrite(LED_PIN, HIGH);
}
 
void loop() {
#if DEBUG_MSG_FUNCTIONS == 1
    DigiKeyboard.println("function loop()");
#endif
    bufferAppendNew();
    Buttons but = readBuffer();
#if DEBUG_MSG_VALUES == 1
    DigiKeyboard.print("but: ");
    DigiKeyboard.println(String(but));
    DigiKeyboard.print("buttonPressed: ");
    DigiKeyboard.println(String(buttonPressed));
#endif
    if(buttonPressed == noButton){
            if(but != error && but != noButton){
                buttonPressed = but;
                digitalWrite(LED_PIN, LOW);
#if TRIGGER_ON_RELEASE == 0
                buttonAction(buttonPressed);
#endif
            }
    } else {
	        if(but == noButton){
#if TRIGGER_ON_RELEASE == 1
                buttonAction(buttonPressed);
#endif
	            buttonPressed = noButton;
                digitalWrite(LED_PIN, HIGH);
	        }
    }
}
