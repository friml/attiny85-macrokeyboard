
# Klikator3000
![img](img/IMG_20190607_211351.jpg)
Simple macro keyboard.

This repository contains code, gerber files for Klikator3000 board, 3D model and stl files of box.

## Configuration
Every button has its own function in `attiny85-macroKeyboard.ino` called *buttonNfunction*, where *N* is number of button.

On Klikator3000 board, buttons are numbered like this:

| | | |
|-|-|-|
|9|8|7|
|6|5|4|
|3|2|1|
| | | |

Use arduino IDE to build and flash Kikator3000.

## Windows driver installation
Follow [this](https://www.youtube.com/watch?v=MmDBvgrYGZs) awesome manual to get drivers needed for Klikator3000 to work with Windows.
## TODO:
- [ ] Remove USB connector error on board
- [ ] Remove unused components
- [ ] Use only round buttons
- [ ] Add ISP connector

## FAQ
- Don't hesitate to ask a question using gitlab issues
